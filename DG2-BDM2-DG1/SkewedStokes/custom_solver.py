"""
This defines a coupled solver for the Stokes equation.

A DG-SIP method is used with weak boundary conditions which should work
well for CG and DG function spaces. A BDM-like projection is included
and this can be used as a post-processing operator to make the DG
velocities exactly divergence free (following Cockburn, 2009).
"""
import numpy
import dolfin
from dolfin import grad, dot, jump, avg
from ocellaris.solvers import Solver, register_solver
from ocellaris.solver_parts import VelocityBDMProjection, navier_stokes_stabilization_penalties
from ocellaris.utils import timeit, linear_solver_from_input, create_vector_functions


# Default values, can be changed in the input file
FIX_PRESSURE_DOF = True


@register_solver('Custom')
class CustomSolver(Solver):
    def __init__(self, simulation):
        self.simulation = sim = simulation
        self.read_input()
        self.create_functions()
        self.create_weak_form()

        sim.log.info('    Using velocity BDM postprocessor: %r' % self.velocity_postprocessing)
        self.velocity_postprocessor = None
        if self.velocity_postprocessing:
            self.velocity_postprocessor = VelocityBDMProjection(sim, sim.data['u'])

        if self.fix_pressure_dof:
            pdof = get_global_row_number(self.subspaces[-1])
            self.pressure_row_to_fix = numpy.array([pdof], dtype=numpy.intc)

    def read_input(self):
        """
        Read the input file values
        """
        sim = self.simulation

        # Solver for the coupled system
        self.coupled_solver = linear_solver_from_input(sim, 'solver/coupled')

        # Remove null space via PETSc or just normalize after solving
        self.fix_pressure_dof = sim.input.get_value(
            'solver/fix_pressure_dof', FIX_PRESSURE_DOF, 'bool'
        )
        self.remove_null_space = not self.fix_pressure_dof

        # No need for any tricks if the pressure has Dirichlet BCs
        if sim.data['dirichlet_bcs'].get('p', []):
            self.remove_null_space = False
            self.fix_pressure_dof = False

        # Local DG velocity postprocessing
        vel_pp = sim.input.get_value('solver/velocity_postprocessing', 'BDM', 'string')
        self.velocity_postprocessing = vel_pp == 'BDM'

    def create_functions(self):
        """
        Create functions to hold solutions
        """
        sim = self.simulation

        # Function spaces
        Vu = sim.data['Vu']
        Vp = sim.data['Vp']

        # Create coupled mixed function space and mixed function
        func_spaces = [Vu] * sim.ndim + [Vp]
        self.subspace_names = ['u%d' % d for d in range(sim.ndim)] + ['p']
        e_mixed = dolfin.MixedElement([fs.ufl_element() for fs in func_spaces])
        Vcoupled = dolfin.FunctionSpace(sim.data['mesh'], e_mixed)
        sim.data['Vcoupled'] = Vcoupled
        sim.ndofs += Vcoupled.dim()

        # Create function assigner to go from mixed to segregated functions
        Nspace = len(func_spaces)
        self.subspaces = [Vcoupled.sub(i) for i in range(Nspace)]
        sim.data['coupled'] = self.coupled_func = dolfin.Function(Vcoupled)
        self.assigner = dolfin.FunctionAssigner(func_spaces, Vcoupled)

        # Create segregated functions on component and vector form
        create_vector_functions(sim, 'u', 'u%d', Vu)
        sim.data['p'] = dolfin.Function(Vp)
        sim.data['ui_tmp'] = dolfin.Function(Vu)

    def create_weak_form(self):
        """
        Create the weak form. The time coefficients correspond to BDF1
        (backward Euler), but this is changed in the time loop. When
        multiple previous time step values are known then BDF2 is used.
        """
        self.eqs = CoupledEquations(self.simulation)

    @timeit
    def postprocess_velocity(self):
        """
        Apply a post-processing operator to the given velocity field
        """
        if self.velocity_postprocessor:
            self.velocity_postprocessor.run()

    @timeit
    def solve_coupled(self):
        """
        Solve the coupled equations
        """
        # Assemble the equation system
        A = self.eqs.assemble_lhs()
        b = self.eqs.assemble_rhs()

        # Treat the pressure null space (oly if not Dirichlet BCs on p)
        if self.fix_pressure_dof:
            A.ident(self.pressure_row_to_fix)
        elif self.remove_null_space:
            if self.pressure_null_space is None:
                # Create null space vector in Vp Space
                null_func = dolfin.Function(self.simulation.data['Vp'])
                null_vec = null_func.vector()
                null_vec[:] = 1
                null_vec *= 1 / null_vec.norm("l2")

                # Convert null space vector to coupled space
                null_func2 = dolfin.Function(self.simulation.data['Vcoupled'])
                ndim = self.simulation.ndim
                fa = dolfin.FunctionAssigner(self.subspaces[ndim], self.simulation.data['Vp'])
                fa.assign(null_func2.sub(ndim), null_func)

                # Create the null space basis
                ns = dolfin.VectorSpaceBasis([null_func2.vector()])
                self.pressure_null_space = ns

            # Make sure the null space is set on the matrix
            dolfin.as_backend_type(A).set_nullspace(self.pressure_null_space)

            # Orthogonalize b with respect to the null space
            self.pressure_null_space.orthogonalize(b)

        # Solve the equation system
        self.coupled_solver.solve(A, self.coupled_func.vector(), b)

        # Assign into the regular (split) functions from the coupled function
        funcs = [self.simulation.data[name] for name in self.subspace_names]
        self.assigner.assign(funcs, self.coupled_func)

        # Removing the null space of the matrix system is not exactly the same
        # as removing the proper null space of the analytical operator. Here
        # we correct for that
        if self.fix_pressure_dof or self.remove_null_space:
            p = self.simulation.data['p']
            dx = dolfin.dx(domain=p.function_space().mesh())

            # Compute mesh volume
            vol = dolfin.assemble(dolfin.Constant(1) * dx)

            # Perform correction multiple times due to round-of error. The
            # first correction can be i.e 1e14 while the next correction is
            # around unity
            pavg = 1e10
            while abs(pavg) > 1000:
                pavg = dolfin.assemble(p * dx) / vol
                p.vector()[:] -= pavg

    @timeit.named('run custom solver')
    def run(self):
        """
        Run the simulation defined on the input file using this custom solver
        """
        sim = self.simulation
        sim.hooks.simulation_started()

        # There is only one "time step"
        self.simulation.hooks.new_timestep(1, 1, 1)

        # Solve for the new time step
        self.solve_coupled()

        # Postprocess the solution velocity field
        # self.postprocess_velocity()

        # Show info about the solution in this time step
        sim.hooks.end_timestep()

        # We are done
        sim.hooks.simulation_ended(success=True)


def get_global_row_number(V):
    """
    Get the lowest global matrix row number belonging to the
    function space V and local cell 0
    If V is a not a subspace then 0 will normally be returned
    (since dof 0 will typically belong to cell 0)
    """
    dm = V.dofmap()
    dof = dm.cell_dofs(0).min()
    gdof = dm.local_to_global_index(dof)
    return dolfin.MPI.min(dolfin.MPI.comm_world, int(gdof))


class CoupledEquations(object):
    def __init__(self, simulation):
        self.simulation = simulation
        self.define_coupled_equation()

    def define_coupled_equation(self):
        """
        Setup the coupled Navier-Stokes equation

        This implementation assembles the full LHS and RHS each time
        they are needed.
        """
        Vcoupled = self.simulation.data['Vcoupled']

        # Unpack the coupled trial and test functions
        uc = dolfin.TrialFunction(Vcoupled)
        vc = dolfin.TestFunction(Vcoupled)
        ulist = []
        vlist = []
        ndim = self.simulation.ndim
        for d in range(ndim):
            ulist.append(uc[d])
            vlist.append(vc[d])

        u = dolfin.as_vector(ulist)
        v = dolfin.as_vector(vlist)
        p = uc[ndim]
        q = vc[ndim]

        eq = define_dg_equations(u, v, p, q, self.simulation)

        a, L = dolfin.system(eq)
        self.form_lhs = dolfin.Form(a)
        self.form_rhs = dolfin.Form(L)
        self.tensor_lhs = None
        self.tensor_rhs = None

    def assemble_lhs(self):
        if self.tensor_lhs is None:
            self.tensor_lhs = dolfin.assemble(self.form_lhs)
        else:
            dolfin.assemble(self.form_lhs, tensor=self.tensor_lhs)
        return self.tensor_lhs

    def assemble_rhs(self):
        if self.tensor_rhs is None:
            self.tensor_rhs = dolfin.assemble(self.form_rhs)
        else:
            dolfin.assemble(self.form_rhs, tensor=self.tensor_rhs)
        return self.tensor_rhs


def define_dg_equations(u, v, p, q, simulation):
    """
    Define the coupled equations in terms of an UFL weak form
    """
    simulation.log.info('    Creating DG weak form with BCs')
    sim = simulation
    mpm = sim.multi_phase_model
    mesh = sim.data['mesh']
    dx = dolfin.dx(domain=mesh)
    dS = dolfin.dS(domain=mesh)

    g = sim.data['g']
    n = dolfin.FacetNormal(mesh)

    # Fluid properties
    rho = mpm.get_density(0)
    nu = mpm.get_laminar_kinematic_viscosity(0)
    mu = mpm.get_laminar_dynamic_viscosity(0)

    # Start building the coupled equations
    eq = 0

    # Elliptic penalties
    penalty_dS, penalty_ds, _D11, D12 = navier_stokes_stabilization_penalties(sim, nu)

    # Momentum equations
    for d in range(sim.ndim):
        # Divergence free criterion
        # ∇⋅u = 0
        u_hat_p = avg(u[d])
        eq -= u[d] * q.dx(d) * dx
        eq += (u_hat_p + D12[d] * jump(u, n)) * jump(q) * n[d]('+') * dS

        # Diffusion:
        # -∇⋅μ∇u
        eq += mu * dot(grad(u[d]), grad(v[d])) * dx

        # Symmetric Interior Penalty method for -∇⋅μ∇u
        eq -= avg(mu) * dot(n('+'), avg(grad(u[d]))) * jump(v[d]) * dS
        eq -= avg(mu) * dot(n('+'), avg(grad(v[d]))) * jump(u[d]) * dS

        # Symmetric Interior Penalty coercivity term
        eq += penalty_dS * jump(u[d]) * jump(v[d]) * dS

        # Pressure
        # ∇p
        eq -= p * v[d].dx(d) * dx
        eq += (avg(p) - dot(D12, jump(p, n))) * jump(v[d]) * n[d]('+') * dS

        # Body force (gravity)
        # ρ g
        eq -= rho * g[d] * v[d] * dx

        # The BCs are imposed for each velocity component u[d] separately
        use_grad_q_form = True
        use_grad_p_form = False
        eq += add_dirichlet_bcs(
            sim, d, u, p, v, q, rho, mu, n, penalty_ds, use_grad_q_form, use_grad_p_form
        )
        eq += add_neumann_bcs(
            sim, d, u, p, v, q, rho, mu, n, penalty_ds, use_grad_q_form, use_grad_p_form
        )
    return eq


def add_dirichlet_bcs(sim, d, u, p, v, q, rho, mu, n, penalty_ds, use_grad_q_form, use_grad_p_form):
    """
    Dirichlet boundary conditions for one velocity component
    Nitsche method, see, e.g, Epshteyn and Rivière (2007),
    "Estimation of penalty parameters for SIPG"
    """
    dirichlet_bcs = sim.data['dirichlet_bcs'].get('u%d' % d, [])
    eq = 0
    for dbc in dirichlet_bcs:
        u_bc = dbc.func()

        # Divergence free criterion
        if use_grad_q_form:
            eq += q * u_bc * n[d] * dbc.ds()
        else:
            eq -= q * u[d] * n[d] * dbc.ds()
            eq += q * u_bc * n[d] * dbc.ds()

        # From IBP of the main equation
        eq -= mu * dot(n, grad(u[d])) * v[d] * dbc.ds()

        # Test functions for the Dirichlet BC
        z1 = penalty_ds * v[d]
        z2 = -mu * dot(n, grad(v[d]))

        # Dirichlet BC added twice with different test functions
        # This is SIPG for -∇⋅μ∇u
        for z in [z1, z2]:
            eq += u[d] * z * dbc.ds()
            eq -= u_bc * z * dbc.ds()

        # Pressure
        if not use_grad_p_form:
            eq += p * v[d] * n[d] * dbc.ds()
    return eq


def add_neumann_bcs(sim, d, u, p, v, q, rho, mu, n, penalty_ds, use_grad_q_form, use_grad_p_form):
    """
    Neumann boundary conditions for one velocity component
    """
    neumann_bcs = sim.data['neumann_bcs'].get('u%d' % d, [])
    eq = 0
    for nbc in neumann_bcs:
        # Divergence free criterion
        if use_grad_q_form:
            eq += q * u[d] * n[d] * nbc.ds()
        else:
            eq -= q * u[d] * n[d] * nbc.ds()

        # Diffusion
        eq -= mu * nbc.func() * v[d] * nbc.ds()

        # Pressure
        if not use_grad_p_form:
            eq += p * v[d] * n[d] * nbc.ds()
    return eq
