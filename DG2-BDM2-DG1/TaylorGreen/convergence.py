import time
from math import log
import dolfin
from ocellaris import Simulation, setup_simulation, run_simulation


def run_and_calculate_error(N, dt, tmax, polydeg_u, polydeg_p):
    """
    Run Ocellaris and return L2 & H1 errors in the last time step
    """
    say(N, dt, tmax, polydeg_u, polydeg_p)

    # Setup and run simulation
    sim = Simulation()
    sim.input.read_yaml('taylor-green.inp')
    sim.input.set_value('user_code/constants/N', N)
    sim.input.set_value('user_code/constants/dt_val', dt)
    sim.input.set_value('user_code/constants/tmax', tmax)
    sim.input.set_value('user_code/constants/Pu', polydeg_u)
    sim.input.set_value('user_code/constants/Pp', polydeg_p)
    sim.input.set_value('output/stdout_enabled', False)

    # Setup and run the solver
    solver_type = sim.input.get_value('solver/type')
    u_fspace = sim.input.get_value('solver/function_space_velocity', 'DG')
    say('Running with %s %s solver ...' % (solver_type, u_fspace))
    t1 = time.time()
    setup_simulation(sim)
    say('Num unknowns', sim.ndofs)
    run_simulation(sim)
    duration = time.time() - t1
    say('DONE')

    # The numerical function spaces and solutions
    Vu = sim.data['Vu']
    Vp = sim.data['Vp']
    u0 = sim.data['u0']
    u1 = sim.data['u1']
    p = sim.data['p']

    # Project the analytical solution into Vu and Vp
    vals = dict(t=sim.time,
                dt=sim.dt,
                nu=sim.input['physical_properties']['nu'],
                rho=sim.input['physical_properties']['rho'])
    u0e = dolfin.Expression(
        sim.input.get_value('initial_conditions/up0/cpp_code'),
        degree=polydeg_u + 3,
        **vals)
    u1e = dolfin.Expression(
        sim.input.get_value('initial_conditions/up1/cpp_code'),
        degree=polydeg_u + 3,
        **vals)
    pe = dolfin.Expression(
        sim.input.get_value('initial_conditions/p/cpp_code'),
        degree=polydeg_p + 3,
        **vals)
    u0a = dolfin.project(u0e, Vu)
    u1a = dolfin.project(u1e, Vu)
    pa = dolfin.project(pe, Vp)

    # Calculate L2 errors
    err_u0 = calc_err(u0, u0a)
    err_u1 = calc_err(u1, u1a)
    err_p = calc_err(p, pa)

    # Calculate H1 errors
    err_u0_H1 = calc_err(u0, u0a, 'H1')
    err_u1_H1 = calc_err(u1, u1a, 'H1')
    err_p_H1 = calc_err(p, pa, 'H1')

    # Compute velocity divergence errors in various function spaces
    div_u_Vp = absmax(dolfin.project(dolfin.div(sim.data['u']), Vp))
    div_u_Vu = absmax(dolfin.project(dolfin.div(sim.data['u']), Vu))
    Vdg0 = dolfin.FunctionSpace(sim.data['mesh'], "DG", 0)
    Vdg1 = dolfin.FunctionSpace(sim.data['mesh'], "DG", 1)
    div_u_DG0 = absmax(dolfin.project(dolfin.div(sim.data['u']), Vdg0))
    div_u_DG1 = absmax(dolfin.project(dolfin.div(sim.data['u']), Vdg1))

    # Some extra info we want to show
    loglines = sim.log.get_full_log().split('\n')
    niter = sum(1 if 'iteration' in line else 0 for line in loglines)
    int_p = dolfin.assemble(sim.data['p'] * dolfin.dx)

    # Print some info about the results
    say('Number of time steps:', sim.timestep)
    say('Num inner iterations:', niter)
    say('Number of mesh cells:', sim.data['mesh'].num_cells())
    say('p*dx', int_p)
    say('div(u)|Vp', div_u_Vp)
    say('div(u)|Vu', div_u_Vu)
    say('div(u)|DG0', div_u_DG0)
    say('div(u)|DG1', div_u_DG1)

    hmin = sim.data['mesh'].hmin()
    return err_u0, err_u1, err_p, err_u0_H1, err_u1_H1, err_p_H1, hmin, dt, duration


def calc_err(f_num, f_ana, normtype='l2'):
    """
    Calculate scaled L2 or H1 error
    """
    f_err = dolfin.Function(f_num.function_space())
    f_err.vector()[:] = f_ana.vector()[:] - f_num.vector()[:]
    if normtype == 'l2':
        return dolfin.norm(f_err) / dolfin.norm(f_ana)
    else:
        return dolfin.norm(f_err, normtype)


def print_results(results, indices, restype):
    for normname, selected in [('L2', slice(0, 3)),
                               ('H1', slice(3, 6))]:
        say('======= ========== ========== ========== ===== ===== ===== ========= =====')
        say(' Discr.        Errors in %s norm         Convergence rates     Duration   ' % normname)
        say('------- -------------------------------- ----------------- ---------------')
        say('    %3s         u0         u1          p    u0    u1     p wallclock  rate' % restype)
        say('======= ========== ========== ========== ===== ===== ===== ========= =====')
        for i, idx in enumerate(indices):
            if idx not in results:
                break
            hmin, dt, duration = results[idx][-3:]
            eu0, eu1, ep = results[idx][selected]
            discr = hmin if restype == 'h' else dt
            say('%7.5f %10.2e %10.2e %10.2e' % (discr, eu0, eu1, ep), end=' ')
            if i > 0:
                prev_idx = indices[i - 1]
                prev_eu0, prev_eu1, prev_ep = results[prev_idx][selected]
                prev_hmin, prev_dt, prev_duration = results[prev_idx][-3:]
                prev_discr = prev_hmin if restype == 'h' else prev_dt
                fac = log(prev_discr / discr)
                say('%5.2f %5.2f %5.2f' % (log(prev_eu0 / eu0) / fac,
                                           log(prev_eu1 / eu1) / fac, log(prev_ep / ep) / fac), end=' ')
                say('%9s %5.2f' % (seconds_as_string(duration), log(duration / prev_duration) / fac))
            else:
                say('                  %9s' % seconds_as_string(duration))

        say('======= ========== ========== ========== ===== ===== ===== ========= =====')
        say()


def seconds_as_string(seconds):
    mins, secs = seconds // 60, seconds % 60
    if not mins:
        return '%4.1fs' % secs
    else:
        return '%2dm %4.1fs' % (mins, secs)


def say(*args, **kwargs):
    if dolfin.MPI.rank(dolfin.MPI.comm_world) == 0:
        print(*args, **kwargs)


def absmax(func):
    this_absmax = abs(func.vector().get_local()).max()
    return dolfin.MPI.max(dolfin.MPI.comm_world, float(this_absmax))


def run_convergence_space(N_list):
    dt = 0.01
    tmax = 1.0
    results = {}
    for N in N_list:
        say('Running N = %g with dt = %g' % (N, dt))
        results[N] = run_and_calculate_error(
            N=N, dt=dt, tmax=tmax, polydeg_u=2, polydeg_p=1)
        print_results(results, N_list, 'h')


if __name__ == '__main__':
    run_convergence_space([8, 16, 24, 32, 40])
