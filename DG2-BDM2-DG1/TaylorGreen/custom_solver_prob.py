"""
This defines a coupled solver for the incompressible Navier-Stokes
equations. A DG-SIP method is used with weak boundary conditions.
Should work well for CG and DG function spaces. Includes a BDM
projection that can be used to make DG velocities exactly divergence
free (following Cockburn, 2009)
"""
import dolfin
from dolfin import Constant, div, grad, dot, jump, avg
from ocellaris.solvers import register_solver
from ocellaris.solver_parts import navier_stokes_stabilization_penalties
from ocellaris.utils import create_vector_functions
from custom_solver import CustomSolver


@register_solver('PRob')
class PRobSolver(CustomSolver):
    def create_functions(self):
        """
        Create functions to hold solutions
        """
        sim = self.simulation

        # Function spaces
        Vu = sim.data['Vu']
        Vp = sim.data['Vp']

        # Create function space Va ∈ H(div)
        mesh = Vu.mesh()
        Pa = Vu.ufl_element().degree()
        Va = dolfin.FunctionSpace(mesh, 'BDM', Pa)
        sim.data['Va'] = Va

        # Create coupled mixed function space and mixed function
        # Va is a vector element, so it is only included once
        func_spaces = [Vu] * sim.ndim + [Va, Vp]
        self.subspace_names = ['u%d' % d for d in range(sim.ndim)] + ['a', 'p']
        e_mixed = dolfin.MixedElement([fs.ufl_element() for fs in func_spaces])
        Vcoupled = dolfin.FunctionSpace(sim.data['mesh'], e_mixed)
        sim.data['Vcoupled'] = Vcoupled
        sim.ndofs += Vcoupled.dim()

        # Create function assigner to go from mixed to segregated functions
        Nspace = len(func_spaces)
        self.subspaces = [Vcoupled.sub(i) for i in range(Nspace)]
        sim.data['coupled'] = self.coupled_func = dolfin.Function(Vcoupled)
        self.assigner = dolfin.FunctionAssigner(func_spaces, Vcoupled)

        # Create segregated functions on component and vector form
        create_vector_functions(sim, 'u', 'u%d', Vu)
        create_vector_functions(sim, 'up', 'up%d', Vu)
        create_vector_functions(sim, 'upp', 'upp%d', Vu)
        create_vector_functions(sim, 'u_conv', 'u_conv%d', Vu)
        create_vector_functions(sim, 'up_conv', 'up_conv%d', Vu)
        create_vector_functions(sim, 'upp_conv', 'upp_conv%d', Vu)
        sim.data['a'] = dolfin.Function(Va)
        sim.data['p'] = dolfin.Function(Vp)
        sim.data['ui_tmp'] = dolfin.Function(Vu)

    def create_weak_form(self):
        """
        Create the weak form. The time coefficients correspond to BDF1
        (backward Euler), but this is changed in the time loop. When
        multiple previous time step values are known then BDF2 is used.
        """
        self.simulation.data['time_coeffs'] = dolfin.Constant([1, -1, 0])
        self.simulation.data['dt'] = Constant(self.simulation.dt)
        self.eqs = CoupledEquationsProb(self.simulation, self.stress_divergence)


class CoupledEquationsProb(object):
    def __init__(self, simulation, use_stress_divergence_form):
        self.simulation = simulation
        self.stress_divergence = use_stress_divergence_form
        self.define_coupled_equation()

    def define_coupled_equation(self):
        """
        Setup the coupled Navier-Stokes equation

        This implementation assembles the full LHS and RHS each time
        they are needed.

        The additional velocity field a is added with test function b.
        This should be from a H(div) conforming space such as BDM
        """
        # Get the coupled trial and test functions
        Vcoupled = self.simulation.data['Vcoupled']
        uc = dolfin.TrialFunction(Vcoupled)
        vc = dolfin.TestFunction(Vcoupled)
        ndim = self.simulation.ndim
        assert len(uc) == 2 * ndim + 1

        # Unpack the coupled trial and test functions
        ulist, alist = [], []
        vlist, blist = [], []
        for d in range(ndim):
            ulist.append(uc[d])
            vlist.append(vc[d])
            alist.append(uc[d + ndim])
            blist.append(vc[d + ndim])

        # Create vectors such that dot et al work properly
        u = dolfin.as_vector(ulist)
        v = dolfin.as_vector(vlist)
        a = dolfin.as_vector(alist)
        b = dolfin.as_vector(blist)
        p = uc[-1]
        q = vc[-1]

        eq = define_dg_equations(u, v, a, b, p, q, self.simulation,
                                 stress_divergence=self.stress_divergence)

        a, L = dolfin.system(eq)
        self.form_lhs = dolfin.Form(a)
        self.form_rhs = dolfin.Form(L)
        self.tensor_lhs = None
        self.tensor_rhs = None

    def assemble_lhs(self):
        if self.tensor_lhs is None:
            self.tensor_lhs = dolfin.assemble(self.form_lhs)
        else:
            dolfin.assemble(self.form_lhs, tensor=self.tensor_lhs)
        return self.tensor_lhs

    def assemble_rhs(self):
        if self.tensor_rhs is None:
            self.tensor_rhs = dolfin.assemble(self.form_rhs)
        else:
            dolfin.assemble(self.form_rhs, tensor=self.tensor_rhs)
        return self.tensor_rhs


def define_dg_equations(u, v, a, b, p, q, simulation, stress_divergence):
    """
    Define the coupled equations in terms of an UFL weak form
    """
    simulation.log.info('    Creating DG weak form with BCs')
    sim = simulation
    mpm = sim.multi_phase_model
    mesh = sim.data['mesh']
    u_conv = sim.data['u_conv']
    dx = dolfin.dx(domain=mesh)
    dS = dolfin.dS(domain=mesh)

    c1, c2, c3 = sim.data['time_coeffs']
    dt = sim.data['dt']
    g = sim.data['g']
    n = dolfin.FacetNormal(mesh)

    # Fluid properties
    rho = mpm.get_density(0)
    nu = mpm.get_laminar_kinematic_viscosity(0)
    mu = mpm.get_laminar_dynamic_viscosity(0)

    # Start building the coupled equations
    eq = 0

    # The u and a velocities are equal (up to a projection)
    eq += dot(u, v) * dx - dot(a, v) * dx
    eq += dot(a, b) * dx - dot(u, b) * dx

    # Elliptic penalties
    penalty_dS, penalty_ds, _D11, D12 = \
        navier_stokes_stabilization_penalties(sim, nu)

    # Upwind and downwind velocities
    w_nU = (dot(u_conv, n) + abs(dot(u_conv, n))) / 2.0
    w_nD = (dot(u_conv, n) - abs(dot(u_conv, n))) / 2.0

    # Momentum equations
    for d in range(sim.ndim):
        up = sim.data['up%d' % d]
        upp = sim.data['upp%d' % d]

        # Divergence free criterion
        # ∇⋅u = 0
        u_hat_p = avg(u[d])
        eq -= u[d] * q.dx(d) * dx
        eq += (u_hat_p + D12[d] * jump(u, n)) * jump(q) * n[d]('+') * dS

        # Time derivative
        # ∂(ρu)/∂t
        eq += rho * (c1 * u[d] + c2 * up + c3 * upp) / dt * v[d] * dx

        # Convection:
        # -w⋅∇(ρu)
        flux_nU = a[d] * w_nU
        flux = jump(flux_nU)
        eq -= a[d] * dot(grad(rho * b[d]), u_conv) * dx
        eq += flux * jump(rho * b[d]) * dS

        # Stabilizing term when w is not divergence free
        eq += 1 / 2 * div(u_conv) * u[d] * v[d] * dx

        # Diffusion:
        # -∇⋅μ∇u
        eq += mu * dot(grad(u[d]), grad(v[d])) * dx

        # Symmetric Interior Penalty method for -∇⋅μ∇u
        eq -= avg(mu) * dot(n('+'), avg(grad(u[d]))) * jump(v[d]) * dS
        eq -= avg(mu) * dot(n('+'), avg(grad(v[d]))) * jump(u[d]) * dS

        # Symmetric Interior Penalty coercivity term
        eq += penalty_dS * jump(u[d]) * jump(v[d]) * dS

        # Stress divergence terms
        # -∇⋅μ(∇u)^T
        if stress_divergence:
            eq += mu * dot(u.dx(d), grad(v[d])) * dx
            eq -= avg(mu) * dot(n('+'), avg(u.dx(d))) * jump(v[d]) * dS
            eq -= avg(mu) * dot(n('+'), avg(v.dx(d))) * jump(u[d]) * dS

        # Pressure
        # ∇p
        eq -= p * v[d].dx(d) * dx
        eq += (avg(p) - dot(D12, jump(p, n))) * jump(v[d]) * n[d]('+') * dS

        # Body force (gravity)
        # ρ g
        eq -= rho * g[d] * v[d] * dx

        # The BCs are imposed for each velocity component u[d] separately
        use_grad_q_form = True
        use_grad_p_form = False
        eq += add_dirichlet_bcs(sim, d, u, a, p, v, b, q, rho, mu, n, w_nU, w_nD,
                                penalty_ds, use_grad_q_form, use_grad_p_form)
        eq += add_neumann_bcs(sim, d, u, a, p, v, b, q, rho, mu, n, w_nU, w_nD,
                              penalty_ds, use_grad_q_form, use_grad_p_form)
    return eq


def add_dirichlet_bcs(sim, d, u, a, p, v, b, q, rho, mu, n, w_nU, w_nD,
                      penalty_ds, use_grad_q_form, use_grad_p_form):
    """
    Dirichlet boundary conditions for one velocity component
    Nitsche method, see, e.g, Epshteyn and Rivière (2007),
    "Estimation of penalty parameters for SIPG"
    """
    dirichlet_bcs = sim.data['dirichlet_bcs'].get('u%d' % d, [])
    eq = 0
    for dbc in dirichlet_bcs:
        u_bc = dbc.func()

        # Divergence free criterion
        if use_grad_q_form:
            eq += q * u_bc * n[d] * dbc.ds()
        else:
            eq -= q * u[d] * n[d] * dbc.ds()
            eq += q * u_bc * n[d] * dbc.ds()

        # Convection
        eq += rho * a[d] * w_nU * b[d] * dbc.ds()
        eq += rho * u_bc * w_nD * b[d] * dbc.ds()

        # From IBP of the main equation
        eq -= mu * dot(n, grad(u[d])) * v[d] * dbc.ds()

        # Test functions for the Dirichlet BC
        z1 = penalty_ds * v[d]
        z2 = -mu * dot(n, grad(v[d]))

        # Dirichlet BC added twice with different test functions
        # This is SIPG for -∇⋅μ∇u
        for z in [z1, z2]:
            eq += u[d] * z * dbc.ds()
            eq -= u_bc * z * dbc.ds()

        # Pressure
        if not use_grad_p_form:
            eq += p * v[d] * n[d] * dbc.ds()
    return eq


def add_neumann_bcs(sim, d, u, a, p, v, b, q, rho, mu, n, w_nU, w_nD,
                    penalty_ds, use_grad_q_form, use_grad_p_form):
    """
    Neumann boundary conditions for one velocity component
    """
    neumann_bcs = sim.data['neumann_bcs'].get('u%d' % d, [])
    eq = 0
    for nbc in neumann_bcs:
        # Divergence free criterion
        if use_grad_q_form:
            eq += q * u[d] * n[d] * nbc.ds()
        else:
            eq -= q * u[d] * n[d] * nbc.ds()

        # Convection
        eq += rho * a[d] * w_nU * b[d] * nbc.ds()

        # Diffusion
        eq -= mu * nbc.func() * v[d] * nbc.ds()

        # Pressure
        if not use_grad_p_form:
            eq += p * v[d] * n[d] * nbc.ds()
    return eq
